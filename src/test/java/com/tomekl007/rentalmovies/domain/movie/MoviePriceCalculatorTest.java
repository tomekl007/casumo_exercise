package com.tomekl007.rentalmovies.domain.movie;


import com.tomekl007.rentalmovies.config.MoviePriceSettings;
import com.tomekl007.rentalmovies.domain.rent.Rental;
import org.junit.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MoviePriceCalculatorTest {
    private ZoneId UTC = ZoneId.of("UTC");
    private MoviePriceSettings moviePriceSettings = new MoviePriceSettings(40, 30);
    private MoviePriceCalculator moviePriceCalculator = new MoviePriceCalculator(
            moviePriceSettings,
            () -> ZonedDateTime.now(ZoneId.of("UTC"))
    );

    @Test
    public void shouldCalculatePriceForNewReleaseForOneDay() {
        //when
        long price =
                moviePriceCalculator.calculatePriceForMovieType(
                        MovieType.PREMIUM,
                        1
                );
        //then
        assertThat(price).isEqualTo(40);
    }

    @Test
    public void shouldCalculatePriceForRegularOn5Days() {
        //when
        long price =
                moviePriceCalculator.calculatePriceForMovieType(
                        MovieType.REGULAR,
                        5
                );
        //then
        assertThat(price).isEqualTo(90);
    }


    @Test
    public void shouldCalculatePriceForRegularOn2Days() {
        //when
        long price =
                moviePriceCalculator.calculatePriceForMovieType(
                        MovieType.REGULAR,
                        2
                );
        //then
        assertThat(price).isEqualTo(30);
    }

    @Test
    public void shouldCalculatePriceForOldOn7Days() {
        //when
        long price =
                moviePriceCalculator.calculatePriceForMovieType(
                        MovieType.OLD,
                        7
                );
        //then
        assertThat(price).isEqualTo(90);
    }

    @Test
    public void shouldCalculateOldPriceForMovieThatWasNotReturnedLate() {
        //given
        ZonedDateTime rentTime = ZonedDateTime.now(UTC);
        MoviePriceCalculator moviePriceCalculator = new MoviePriceCalculator(
                moviePriceSettings,
                () -> rentTime.plusDays(1)
        );

        List<Rental> rentals = Arrays.asList(new Rental(
                1,
                rentTime,
                new Movie("X", MovieType.REGULAR))
        );

        //when
        long price =
                moviePriceCalculator.calculateLateReturn(
                        rentals
                );

        //then
        assertThat(price).isEqualTo(0);
    }

    @Test
    public void shouldCalculateREGULARPriceForMovieThatWasReturnedLate1Day() {
        //given
        ZonedDateTime rentTime = ZonedDateTime.now(UTC);
        MoviePriceCalculator moviePriceCalculator = new MoviePriceCalculator(
                moviePriceSettings,
                () -> rentTime.plusDays(2)
        );

        List<Rental> rentals = Arrays.asList(new Rental(
                1,
                rentTime,
                new Movie("X", MovieType.REGULAR))
        );

        //when
        long price =
                moviePriceCalculator.calculateLateReturn(
                        rentals
                );

        //then
        assertThat(price).isEqualTo(Long.valueOf(moviePriceSettings.getBasic()));
    }

    @Test
    public void shouldCalculatePremiumPriceForMovieThatWasReturnedLate1Day() {
        //given
        ZonedDateTime rentTime = ZonedDateTime.now(UTC);
        MoviePriceCalculator moviePriceCalculator = new MoviePriceCalculator(
                moviePriceSettings,
                () -> rentTime.plusDays(2)
        );

        List<Rental> rentals = Arrays.asList(new Rental(
                1,
                rentTime,
                new Movie("X", MovieType.PREMIUM))
        );

        //when
        long price =
                moviePriceCalculator.calculateLateReturn(
                        rentals
                );

        //then
        assertThat(price).isEqualTo(Long.valueOf(moviePriceSettings.getPremium()));
    }

    @Test
    public void shouldCalculatePriceForMultipleMoviesReturnedLate() {
        //given
        ZonedDateTime rentTime = ZonedDateTime.now(UTC);
        MoviePriceCalculator moviePriceCalculator = new MoviePriceCalculator(
                moviePriceSettings,
                () -> rentTime.plusDays(4)
        );

        List<Rental> rentals = Arrays.asList(
                new Rental(
                        1,
                        rentTime,
                        new Movie("X", MovieType.PREMIUM)
                ), //3 * 40 = 120
                new Rental(
                        10,
                        rentTime,
                        new Movie("X", MovieType.PREMIUM)
                ),
                new Rental(
                        1,
                        rentTime,
                        new Movie("X", MovieType.OLD)
                ),//3 * 30 = 90
                new Rental(
                        2,
                        rentTime,
                        new Movie("X", MovieType.REGULAR)
                )//2 * 30 = 60
        );

        //when
        long price =
                moviePriceCalculator.calculateLateReturn(
                        rentals
                );

        //then
        assertThat(price).isEqualTo(270L);
    }

    @Test
    public void shouldCalculatePriceForMovieThatWasReturnedEarilier() {
        //given
        ZonedDateTime rentTime = ZonedDateTime.now(UTC);
        MoviePriceCalculator moviePriceCalculator = new MoviePriceCalculator(
                moviePriceSettings,
                () -> rentTime.plusDays(1)
        );

        List<Rental> rentals = Arrays.asList(new Rental(
                10,
                rentTime,
                new Movie("X", MovieType.REGULAR))
        );

        //when
        long price =
                moviePriceCalculator.calculateLateReturn(
                        rentals
                );

        //then
        assertThat(price).isEqualTo(0);
    }
}