package com.tomekl007.rentalmovies.domain.bonuspoints;


import com.tomekl007.rentalmovies.config.BonusPointsSettings;
import com.tomekl007.rentalmovies.domain.movie.Movie;
import com.tomekl007.rentalmovies.domain.movie.MovieType;
import com.tomekl007.rentalmovies.domain.rent.Rental;
import com.tomekl007.rentalmovies.domain.user.User;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BonusPointsAggregatorTest {

    private BonusPointsSettings bonusPointsSettings = new BonusPointsSettings(2, 1);
    private BonusPointsAggregator bonusPointsAggregator = new BonusPointsAggregator(bonusPointsSettings);

    @Test
    public void shouldCalculateBonusPointsForPremium() {
        //given
        List<Rental> rentals = Arrays.asList(
                new Rental(1,
                        ZonedDateTime.now(),
                        new Movie("a", MovieType.PREMIUM))
        );
        User user = new User(1);

        //when
        bonusPointsAggregator.addPointsForUser(user, rentals);

        //then
        assertThat(user.getBonusPoints()).isEqualTo(bonusPointsSettings.getForPremium());
    }

    @Test
    public void shouldCalculateBonusPointsForRegular() {
        //given
        List<Rental> rentals = Arrays.asList(
                new Rental(1,
                        ZonedDateTime.now(),
                        new Movie("a", MovieType.REGULAR))
        );
        User user = new User(1);

        //when
        bonusPointsAggregator.addPointsForUser(user, rentals);

        //then
        assertThat(user.getBonusPoints()).isEqualTo(bonusPointsSettings.getForOthers());
    }

    @Test
    public void shouldCalculateBonusPointsForMultipleRentals() {
        //given
        List<Rental> rentals = Arrays.asList(
                new Rental(1,
                        ZonedDateTime.now(),
                        new Movie("a", MovieType.REGULAR)),
                new Rental(1,
                        ZonedDateTime.now(),
                        new Movie("a", MovieType.PREMIUM)),
                new Rental(1,
                        ZonedDateTime.now(),
                        new Movie("a", MovieType.OLD))
        );
        User user = new User(1);

        //when
        bonusPointsAggregator.addPointsForUser(user, rentals);

        //then
        assertThat(user.getBonusPoints()).isEqualTo(
                bonusPointsSettings.getForOthers() +
                bonusPointsSettings.getForOthers() +
                bonusPointsSettings.getForPremium()
        );
    }
}