package com.tomekl007.rentalmovies;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Supplier;

@Component
@Profile("integration")
public class ForwardingTimeSupplier implements Supplier<ZonedDateTime> {
    private static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");

    private ZonedDateTime zonedDateTime = ZonedDateTime.now(UTC_ZONE_ID);

    @Override
    public ZonedDateTime get() {
        return zonedDateTime;
    }

    public void setZonedDateTime(ZonedDateTime zonedDateTime) {
        this.zonedDateTime = zonedDateTime;
    }
}
