package com.tomekl007.rentalmovies;

import com.tomekl007.rentalmovies.api.RentRequest;
import com.tomekl007.rentalmovies.api.RentReturnRequest;
import com.tomekl007.rentalmovies.api.RentReturnResponse;
import com.tomekl007.rentalmovies.api.SingleRentRequest;
import com.tomekl007.rentalmovies.config.MoviePriceSettings;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("integration")
public class RentMovieControllerIntegrationTest {
    private ZoneId UTC = ZoneId.of("UTC");

    @Autowired
    private TestRestTemplate restTemplate;


    @Autowired
    ForwardingTimeSupplier forwardingTimeSupplier;

    @Autowired
    MoviePriceSettings moviePriceSettings;

    @Test
    public void shouldRentMovieForUser() {
        //given
        List<SingleRentRequest> rentRequests = Arrays.asList(
                new SingleRentRequest("Matrix", 1)
        );
        RentRequest rentRequest = new RentRequest(rentRequests);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        //when
        ResponseEntity<RentReturnResponse> resposne = restTemplate
                .postForEntity("/rent/" + new Random().nextInt(100_000),
                        rentRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(resposne.getStatusCode().value()).isEqualTo(200);
        assertThat(resposne.getBody().getAmountToPay()).isEqualTo(40);
    }

    @Test
    public void shouldRentMultipleMovieForUser() {
        //given
        List<SingleRentRequest> rentRequests = Arrays.asList(
                new SingleRentRequest("Matrix", 1),
                new SingleRentRequest("Spider-Man", 5),
                new SingleRentRequest("Spider-Man", 2),
                new SingleRentRequest("Out of Africa", 7)

        );
        RentRequest rentRequest = new RentRequest(rentRequests);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        //when
        ResponseEntity<RentReturnResponse> response = restTemplate
                .postForEntity("/rent/" + new Random().nextInt(100_000),
                        rentRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(response.getBody().getAmountToPay()).isEqualTo(250);
    }

    @Test
    public void shouldNotRentNonExistingMovie() {
        //given
        List<SingleRentRequest> rentRequests = Arrays.asList(
                new SingleRentRequest("Non-existing", 3)
        );
        RentRequest rentRequest = new RentRequest(rentRequests);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        //when
        ResponseEntity<String> response = restTemplate
                .postForEntity("/rent/" + new Random().nextInt(100_000),
                        rentRequest,
                        String.class,
                        headers
                );

        //then
        assertThat(response.getStatusCode().value()).isEqualTo(404);
    }

    @Test
    public void shouldCalculateSuperchargeForNonExistingRental() {
        //given
        RentReturnRequest rentReturnRequest = new RentReturnRequest(
                Arrays.asList(
                        "Matrix"
                )
        );
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //when
        ResponseEntity<RentReturnResponse> response = restTemplate
                .postForEntity("/rent/return/" + new Random().nextInt(100_000),
                        rentReturnRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(response.getStatusCode().value()).isEqualTo(200);
        assertThat(response.getBody().getAmountToPay()).isEqualTo(0);
    }

    @Test
    public void shouldNotCalculateChargeForPreviouslyDoneRentalReturnedEarly() {
        //given
        List<SingleRentRequest> rentRequests = Arrays.asList(
                new SingleRentRequest("Matrix", 1)

        );
        String userId = String.valueOf(new Random().nextInt(100_000));
        RentRequest rentRequest = new RentRequest(rentRequests);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        //when
        ResponseEntity<RentReturnResponse> response = restTemplate
                .postForEntity("/rent/" + userId,
                        rentRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(response.getStatusCode().value()).isEqualTo(200);

        //and
        RentReturnRequest rentReturnRequest = new RentReturnRequest(
                Arrays.asList(
                        "Matrix"
                )
        );

        //when
        ResponseEntity<RentReturnResponse> responseReturn = restTemplate
                .postForEntity("/rent/return/" + userId,
                        rentReturnRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(responseReturn.getStatusCode().value()).isEqualTo(200);
        assertThat(responseReturn.getBody().getAmountToPay()).isEqualTo(0);
    }

    @Test
    public void shouldCalculateChargeForPreviouslyDoneRental() {
        //given
        List<SingleRentRequest> rentRequests = Arrays.asList(
                new SingleRentRequest("Matrix", 1)

        );
        String userId = String.valueOf(new Random().nextInt(100_000));
        RentRequest rentRequest = new RentRequest(rentRequests);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        //when
        ResponseEntity<RentReturnResponse> response = restTemplate
                .postForEntity("/rent/" + userId,
                        rentRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(response.getStatusCode().value()).isEqualTo(200);

        //and
        RentReturnRequest rentReturnRequest = new RentReturnRequest(
                Arrays.asList(
                        "Matrix"
                )
        );

        //when
        forwardingTimeSupplier.setZonedDateTime(ZonedDateTime.now(UTC).plusDays(3));
        ResponseEntity<RentReturnResponse> responseReturn = restTemplate
                .postForEntity("/rent/return/" + userId,
                        rentReturnRequest,
                        RentReturnResponse.class,
                        headers
                );

        //then
        assertThat(responseReturn.getStatusCode().value()).isEqualTo(200);
        assertThat(responseReturn.getBody().getAmountToPay()).isEqualTo(
                Long.valueOf(moviePriceSettings.getPremium() * 2)
        );
    }

}


