package com.tomekl007.rentalmovies.persistance.movies;

import com.tomekl007.rentalmovies.domain.movie.Movie;
import com.tomekl007.rentalmovies.domain.movie.MovieType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class InMemoryMoviesRepository implements MoviesRepository {
    //emulate embedded db
    private static Map<String, Movie> availableMovies = new HashMap<>();

    static {
        availableMovies.put("Matrix", new Movie("Matrix", MovieType.PREMIUM));
        availableMovies.put("Spider-Man", new Movie("Spider-Man", MovieType.REGULAR));
        availableMovies.put("Spider-Man 2", new Movie("Spider-Man 2", MovieType.REGULAR));
        availableMovies.put("Out of Africa", new Movie("Out of Africa", MovieType.OLD));
    }

    @Override
    public Optional<Movie> getMovie(String title) {
        return Optional.ofNullable(availableMovies.get(title));
    }
}
