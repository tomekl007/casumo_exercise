package com.tomekl007.rentalmovies.persistance.movies;

import com.tomekl007.rentalmovies.domain.movie.Movie;

import java.util.Optional;

public interface MoviesRepository {
    Optional<Movie> getMovie(String title);
}
