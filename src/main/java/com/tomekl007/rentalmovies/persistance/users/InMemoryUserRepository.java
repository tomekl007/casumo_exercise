package com.tomekl007.rentalmovies.persistance.users;


import com.tomekl007.rentalmovies.domain.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class InMemoryUserRepository implements UsersRepository {
    private static final Logger LOG = LoggerFactory.getLogger(InMemoryUserRepository.class);
    private final Map<Integer, User> users = new HashMap<>();

    @Override
    public Optional<User> getUser(Integer id) {
        users.putIfAbsent(id, new User(id));
        Optional<User> user = Optional.of(users.get(id));
        LOG.info("returning user: {}", user);
        return user;

    }
}
