package com.tomekl007.rentalmovies.persistance.users;




import com.tomekl007.rentalmovies.domain.user.User;

import java.util.Optional;

public interface UsersRepository {
    Optional<User> getUser(Integer id);
}
