package com.tomekl007.rentalmovies.domain.user;


import com.tomekl007.rentalmovies.domain.rent.Rental;

import java.util.LinkedList;
import java.util.List;

public class User {
    private final Integer id;
    private int bonusPoints = 0;
    private final List<Rental> pendingRentals = new LinkedList<>();

    public User(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public int getBonusPoints() {
        return bonusPoints;
    }

    public void addBonusPoint(int points) {
        bonusPoints += points;
    }

    public List<Rental> getPendingRentals() {
        return pendingRentals;
    }

    public void addNewRentals(List<Rental> rentals) {
        pendingRentals.addAll(rentals);

    }

    public void removeRentals(List<Rental> rentalsReturned) {
        pendingRentals.removeAll(rentalsReturned);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", bonusPoints=" + bonusPoints +
                ", pendingRentals=" + pendingRentals +
                '}';
    }
}
