package com.tomekl007.rentalmovies.domain.rent;

import com.tomekl007.rentalmovies.api.RentRequest;
import com.tomekl007.rentalmovies.api.RentReturnRequest;
import com.tomekl007.rentalmovies.domain.bonuspoints.BonusPointsAggregator;
import com.tomekl007.rentalmovies.domain.movie.Movie;
import com.tomekl007.rentalmovies.domain.movie.MoviePriceCalculator;
import com.tomekl007.rentalmovies.domain.movie.MovieType;
import com.tomekl007.rentalmovies.domain.user.User;
import com.tomekl007.rentalmovies.persistance.movies.MoviesRepository;
import com.tomekl007.rentalmovies.persistance.users.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Component
public class RentalService {

    private final MoviesRepository moviesRepository;
    private final Supplier<ZonedDateTime> timeSupplier;
    private final UsersRepository usersRepository;
    private final MoviePriceCalculator moviePriceCalculator;
    private final BonusPointsAggregator bonusPointsAggregator;

    @Autowired
    public RentalService(MoviesRepository moviesRepository,
                         Supplier<ZonedDateTime> timeSupplier,
                         UsersRepository usersRepository,
                         MoviePriceCalculator moviePriceCalculator,
                         BonusPointsAggregator bonusPointsAggregator

    ) {
        this.moviesRepository = moviesRepository;
        this.timeSupplier = timeSupplier;
        this.usersRepository = usersRepository;
        this.moviePriceCalculator = moviePriceCalculator;
        this.bonusPointsAggregator = bonusPointsAggregator;
    }

    public boolean validateThatAllMoviesExists(List<String> movieTitles) {
        return movieTitles.stream().map(moviesRepository::getMovie)
                .filter(Optional::isPresent)
                .count() == movieTitles.size();

    }

    public long createRentForUser(Integer id, RentRequest rentRequest) {
        List<Rental> rentals = mapToRent(rentRequest);

        usersRepository
                .getUser(id)
                .ifPresent(u -> {
                    u.addNewRentals(rentals);
                    bonusPointsAggregator.addPointsForUser(u, rentals);
                });

        return rentals
                .stream()
                .map(r -> moviePriceCalculator.calculatePriceForMovieType(
                        r.getMovie().getType(),
                        r.getNumberOfDaysToRentFor())
                )
                .mapToLong(i -> i)
                .sum();
    }

    private MovieType getTypeOfMovie(String movieTitle) {
        return moviesRepository
                .getMovie(movieTitle)
                .map(Movie::getType)
                .orElseThrow(() -> new IllegalStateException("Trying to get movieType for non-existing movie"));
    }

    private List<Rental> mapToRent(RentRequest rentRequest) {
        return rentRequest
                .getTitlesToRent()
                .stream()
                .map(r -> new Rental(
                                r.getNumberOfDaysToRentFor(),
                                timeSupplier.get(),
                                new Movie(r.getTitle(), getTypeOfMovie(r.getTitle()))
                        )
                ).collect(Collectors.toList());
    }

    public Long returnRental(Integer id, RentReturnRequest rentReturnRequest) {
        Optional<User> user = usersRepository.getUser(id);
        List<Rental> pendingRentals =
                user.map(User::getPendingRentals).orElseGet(Collections::emptyList);

        List<Rental> rentalsReturned = pendingRentals
                .stream()
                .filter(r -> rentReturnRequest
                        .getTitlesToReturn()
                        .contains(
                                r.getMovie().getTitle()
                        )
                ).collect(Collectors.toList());

        user.ifPresent(u -> u.removeRentals(rentalsReturned));

        return moviePriceCalculator.calculateLateReturn(rentalsReturned);

    }
}
