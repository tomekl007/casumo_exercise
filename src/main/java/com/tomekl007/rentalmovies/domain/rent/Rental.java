package com.tomekl007.rentalmovies.domain.rent;


import com.tomekl007.rentalmovies.domain.movie.Movie;

import java.time.ZonedDateTime;

public class Rental {
    private final int numberOfDaysToRentFor;
    private final ZonedDateTime dateOfRent;
    private final Movie movie;

    public Rental(int numberOfDaysToRentFor,
                  ZonedDateTime dateOfRent,
                  Movie movie) {
        this.dateOfRent = dateOfRent;
        this.numberOfDaysToRentFor = numberOfDaysToRentFor;
        this.movie = movie;
    }

    public ZonedDateTime getDateOfRent() {
        return dateOfRent;
    }

    public int getNumberOfDaysToRentFor() {
        return numberOfDaysToRentFor;
    }

    public Movie getMovie() {
        return movie;
    }

    @Override
    public String toString() {
        return "Rental{" +
                "numberOfDaysToRentFor=" + numberOfDaysToRentFor +
                ", dateOfRent=" + dateOfRent +
                ", movie=" + movie +
                '}';
    }
}
