package com.tomekl007.rentalmovies.domain.movie;

public class Movie {
    private final String title;
    private final MovieType type;

    public Movie(String title, MovieType type) {
        this.title = title;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public MovieType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", type=" + type +
                '}';
    }
}
