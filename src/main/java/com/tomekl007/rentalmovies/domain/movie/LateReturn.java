package com.tomekl007.rentalmovies.domain.movie;

public class LateReturn {
    private final MovieType type;
    private final long numberOfDaysRented;
    private final int numberOfDaysDeclared;


    public LateReturn(MovieType type, long numberOfDaysRented, int numberOfDaysDeclared) {
        this.type = type;
        this.numberOfDaysRented = numberOfDaysRented;
        this.numberOfDaysDeclared = numberOfDaysDeclared;
    }

    public MovieType getType() {
        return type;
    }

    public long getNumberOfDaysRented() {
        return numberOfDaysRented;
    }

    public int getNumberOfDaysDeclared() {
        return numberOfDaysDeclared;
    }
}
