package com.tomekl007.rentalmovies.domain.movie;

import com.tomekl007.rentalmovies.config.MoviePriceSettings;
import com.tomekl007.rentalmovies.domain.rent.Rental;
import com.tomekl007.rentalmovies.persistance.users.InMemoryUserRepository;
import io.vavr.API;
import io.vavr.Tuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Supplier;

import static io.vavr.API.$;
import static io.vavr.API.Case;

@Component
public class MoviePriceCalculator {
    private static final int NUMBER_OF_DAYS_FOR_REGULAR = 3;
    private static final int NUMBER_OF_DAYS_FOR_OLD = 5;
    private final MoviePriceSettings moviePriceSettings;
    private final Supplier<ZonedDateTime> timeSupplier;

    @Autowired
    public MoviePriceCalculator(MoviePriceSettings moviePriceSettings,
                                Supplier<ZonedDateTime> timeSupplier) {
        this.moviePriceSettings = moviePriceSettings;
        this.timeSupplier = timeSupplier;
    }

    public long calculatePriceForMovieType(
            MovieType movieType,
            int numberOfDays) {
        return API.Match(movieType).of(
                Case($(MovieType.REGULAR), calculateForRegular(numberOfDays)),
                Case($(MovieType.OLD), calculateForOld(numberOfDays)),
                Case($(MovieType.PREMIUM), calculateForPremium(numberOfDays))
        );
    }

    private long calculateForPremium(int numberOfDays) {
        return moviePriceSettings.getPremium() * numberOfDays;
    }

    private long calculateForOld(int numberOfDays) {
        return priceForNdays(numberOfDays, NUMBER_OF_DAYS_FOR_OLD);
    }

    private long calculateForRegular(int numberOfDays) {
        return priceForNdays(numberOfDays, NUMBER_OF_DAYS_FOR_REGULAR);
    }

    private long priceForNdays(int numberOfDays, int numberOfAllowedDays) {
        if (numberOfDays < numberOfAllowedDays) {
            return moviePriceSettings.getBasic();
        } else {
            return moviePriceSettings.getBasic() +
                    ((numberOfDays - numberOfAllowedDays)
                            * moviePriceSettings.getBasic());
        }
    }

    public Long calculateLateReturn(List<Rental> rentalsReturned) {
        return rentalsReturned.stream()
                .map(r -> new LateReturn(
                                r.getMovie().getType(),
                                calculateDaysBetween(r.getDateOfRent()),
                                r.getNumberOfDaysToRentFor()
                        )
                )
                .map(r -> Tuple.of(r.getType(), calculateLateDays(r)))
                .filter(t -> t._2 > 0)
                .map(t -> t._2 * calculateLatePrice(t._1))
                .mapToLong(i -> i)
                .sum();
    }

    private long calculateLatePrice(MovieType movieType) {
        return API.Match(movieType).of(
                Case($(MovieType.REGULAR), moviePriceSettings.getBasic()),
                Case($(MovieType.OLD), moviePriceSettings.getBasic()),
                Case($(MovieType.PREMIUM), moviePriceSettings.getPremium())
        );
    }

    private Long calculateLateDays(LateReturn lateReturn) {
        return lateReturn.getNumberOfDaysRented() - lateReturn.getNumberOfDaysDeclared();
    }

    private long calculateDaysBetween(ZonedDateTime dateFrom) {
        ChronoUnit days = ChronoUnit.DAYS;
        return days.between(dateFrom, timeSupplier.get());
    }
}
