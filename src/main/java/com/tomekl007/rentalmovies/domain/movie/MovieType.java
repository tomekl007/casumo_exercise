package com.tomekl007.rentalmovies.domain.movie;

public enum MovieType {
    PREMIUM, REGULAR, OLD
}
