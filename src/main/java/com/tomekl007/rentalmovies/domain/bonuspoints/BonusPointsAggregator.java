package com.tomekl007.rentalmovies.domain.bonuspoints;

import com.tomekl007.rentalmovies.config.BonusPointsSettings;
import com.tomekl007.rentalmovies.domain.movie.MovieType;
import com.tomekl007.rentalmovies.domain.rent.Rental;
import com.tomekl007.rentalmovies.domain.user.User;
import io.vavr.API;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static io.vavr.API.$;
import static io.vavr.API.Case;

@Component
public class BonusPointsAggregator {

    private final BonusPointsSettings bonusPointsSettings;

    @Autowired
    public BonusPointsAggregator(BonusPointsSettings bonusPointsSettings) {
        this.bonusPointsSettings = bonusPointsSettings;
    }

    public void addPointsForUser(User user, List<Rental> rentals) {
        user.addBonusPoint(
                rentals
                        .stream()
                        .map(r -> calculateBonusPoints(r.getMovie().getType()))
                        .mapToInt(i -> i)
                        .sum()
        );
    }

    private int calculateBonusPoints(MovieType movieType) {
        return API.Match(movieType).of(
                Case($(MovieType.REGULAR), bonusPointsSettings.getForOthers()),
                Case($(MovieType.OLD), bonusPointsSettings.getForOthers()),
                Case($(MovieType.PREMIUM), bonusPointsSettings.getForPremium())
        );
    }
}
