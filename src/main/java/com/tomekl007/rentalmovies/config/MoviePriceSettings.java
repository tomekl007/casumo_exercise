package com.tomekl007.rentalmovies.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "movie-price")
public class MoviePriceSettings {
    private Integer premium;
    private Integer basic;

    public MoviePriceSettings() {
    }

    public MoviePriceSettings(Integer premium, Integer basic) {
        this.premium = premium;
        this.basic = basic;
    }

    public void setPremium(Integer premium) {
        this.premium = premium;
    }

    public void setBasic(Integer basic) {
        this.basic = basic;
    }

    public Integer getPremium() {
        return premium;
    }

    public Integer getBasic() {
        return basic;
    }
}
