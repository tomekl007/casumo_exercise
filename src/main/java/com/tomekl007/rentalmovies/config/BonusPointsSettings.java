package com.tomekl007.rentalmovies.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "bonus-points")
public class BonusPointsSettings {
    private Integer forPremium;
    private Integer forOthers;

    public BonusPointsSettings(Integer forPremium, Integer forOthers) {
        this.forPremium = forPremium;
        this.forOthers = forOthers;
    }

    public BonusPointsSettings() {
    }

    public Integer getForPremium() {
        return forPremium;
    }

    public void setForPremium(Integer forPremium) {
        this.forPremium = forPremium;
    }

    public Integer getForOthers() {
        return forOthers;
    }

    public void setForOthers(Integer forOthers) {
        this.forOthers = forOthers;
    }
}
