package com.tomekl007.rentalmovies.api;

public class RentReturnResponse {
    private Long amountToPay;

    public RentReturnResponse(Long amountToPay) {
        this.amountToPay = amountToPay;
    }

    public RentReturnResponse() {
    }

    public Long getAmountToPay() {
        return amountToPay;
    }

    public void setAmountToPay(Long amountToPay) {
        this.amountToPay = amountToPay;
    }
}
