package com.tomekl007.rentalmovies.api;

import com.tomekl007.rentalmovies.domain.rent.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.stream.Collectors;

@Controller
public class RentalController {
    private final RentalService rentalService;

    @Autowired
    public RentalController(RentalService rentalService) {
        this.rentalService = rentalService;
    }

    @RequestMapping(value = "/rent/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createRental(
            @PathVariable("id") Integer id,
            @RequestBody RentRequest rentRequest) {
        if (!rentalService.validateThatAllMoviesExists(rentRequest
                .getTitlesToRent()
                .stream()
                .map(SingleRentRequest::getTitle)
                .collect(Collectors.toList())
        )) {
            return ResponseEntity.notFound().build();
        }

        long amountToPay = rentalService.createRentForUser(id, rentRequest);
        return ResponseEntity.ok(new RentReturnResponse(amountToPay));
    }

    @RequestMapping(value = "/rent/return/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RentReturnResponse> returnMovies(
            @PathVariable("id") Integer id,
            @RequestBody RentReturnRequest rentReturnRequest) {
        if (!rentalService.validateThatAllMoviesExists(rentReturnRequest.getTitlesToReturn())) {
            return ResponseEntity.notFound().build();
        }

        Long amountToPay = rentalService.returnRental(id, rentReturnRequest);
        return ResponseEntity.ok(new RentReturnResponse(amountToPay));
    }
}
