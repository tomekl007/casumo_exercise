package com.tomekl007.rentalmovies.api;

import java.util.List;

public class RentReturnRequest {
    private List<String> titlesToReturn;

    public RentReturnRequest(List<String> titlesToReturn) {
        this.titlesToReturn = titlesToReturn;
    }

    public RentReturnRequest() {
    }

    public List<String> getTitlesToReturn() {
        return titlesToReturn;
    }

    public void setTitlesToReturn(List<String> titlesToReturn) {
        this.titlesToReturn = titlesToReturn;
    }


    @Override
    public String toString() {
        return "RentReturnRequest{" +
                "titlesToReturn=" + titlesToReturn +
                '}';
    }
}
