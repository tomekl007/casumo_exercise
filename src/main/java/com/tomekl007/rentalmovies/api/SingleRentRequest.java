package com.tomekl007.rentalmovies.api;

public class SingleRentRequest {
    private Integer numberOfDaysToRentFor;
    private String title;

    public SingleRentRequest(String title, Integer numberOfDaysToRentFor) {
        this.title = title;
        this.numberOfDaysToRentFor = numberOfDaysToRentFor;
    }

    public SingleRentRequest() {
    }

    public Integer getNumberOfDaysToRentFor() {
        return numberOfDaysToRentFor;
    }

    public void setNumberOfDaysToRentFor(Integer numberOfDaysToRentFor) {
        this.numberOfDaysToRentFor = numberOfDaysToRentFor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "SingleRentRequest{" +
                "numberOfDaysToRentFor=" + numberOfDaysToRentFor +
                ", title='" + title + '\'' +
                '}';
    }
}
