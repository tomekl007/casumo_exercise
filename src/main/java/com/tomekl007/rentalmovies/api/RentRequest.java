package com.tomekl007.rentalmovies.api;

import java.util.List;

public class RentRequest {
    private List<SingleRentRequest> titlesToRent;

    public RentRequest() {
    }

    public RentRequest(List<SingleRentRequest> titlesToRent) {
        this.titlesToRent = titlesToRent;
    }

    public List<SingleRentRequest> getTitlesToRent() {
        return titlesToRent;
    }

    public void setTitlesToRent(List<SingleRentRequest> titlesToRent) {
        this.titlesToRent = titlesToRent;
    }

    @Override
    public String toString() {
        return "RentRequest{" +
                "titlesToRent=" + titlesToRent +
                '}';
    }
}
