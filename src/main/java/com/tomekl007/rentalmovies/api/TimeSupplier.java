package com.tomekl007.rentalmovies.api;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.function.Supplier;

@Component
@Profile("!integration")
public class TimeSupplier implements Supplier<ZonedDateTime> {
    private static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");

    @Override
    public ZonedDateTime get() {
        return ZonedDateTime.now(UTC_ZONE_ID);
    }
}
