1. I am assuming that for simplicity of this exercise 
   we are creating application that works only for one (SEK) currency.
   If we want to extends that app to work on other currencies, we 
   would need to add currencies converter component
2. I am assuming that whole project should be in java (including tests)
   If I could use other language and testing framework I would use spock and groovy
   for great and fluent parametrized tests
3. I am assuming that type long will have enough precision for price.
   If we would more precision I would use BigDecimal
4. Business rules for price are hardcoded in MoviePriceCalculator. 
   If we want to configure them dynamically we can move them to database or config file.
5. I am creating repository interface and in memory implementation.
   We can easily change it to database that will be storing all data
6. I am assuming that title is good identifier (Primary Key) for a movie               
7. If rent request contains any movie that does not exists I am returning 404 code. 
   On production there should be more detailed message
8. When user is not present in the embedded db mode, I am creating new one with 0 bonus points
9. In description there is no information about how much cost late return of old move. 
   I am assuming same as for regular
10. I am not doing additional validation. In the production read application
    the validation of number of movies rent by specific user should be added.       
11. There is no concept of stock. In the production ready app there should be 
    number of available copies of a movie that will be rent.
    
To start all tests:

./mvnw clean test

To start application:

./mvnw spring-boot:run

API will be exposed on the http://localhost:8080

Example RENT CALL for user id 1:

POST http://localhost:8080/rent/1

header: Content-Type: application/json

CONTENT:

{
	"titlesToRent": 
	[
		{
			"title": "Matrix",
			"numberOfDaysToRentFor": 1
		}	
	]
}

EXAMPLE RETURN CALL for user id 1:

POST http://localhost:8080/rent/return/1

header: Content-Type: application/json

CONTENT

{
	"titlesToReturn": ["Matrix"]
}
